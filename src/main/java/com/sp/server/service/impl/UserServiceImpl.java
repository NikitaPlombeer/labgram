package com.sp.server.service.impl;

import com.sp.server.dao.UserDAO;
import com.sp.server.entity.User;
import com.sp.server.exception.AlreadySubscribedException;
import com.sp.server.exception.NotSubscribedException;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.repository.UserRepository;
import com.sp.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sharadbrat on 15.02.17.
 */

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    private UserRepository repository;

    @Autowired
    public void setRepository(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User getById(long id) throws UserNotFoundException {
        User user = userDAO.find(id);
        if (user == null)
            throw new UserNotFoundException(id);
        return user;
    }

    @Override
    public List<User> getSubscriptions(long id) throws UserNotFoundException {
        List<User> subscriptions = userDAO.getSubscriptions(id);
        if (subscriptions.isEmpty())
            throw new UserNotFoundException(id);
        return subscriptions;
    }

    @Override
    public List<User> getSubscribers(Long id) throws UserNotFoundException {
        List<User> subscribers = userDAO.getSubscribers(id);
        if (subscribers.isEmpty())
            throw new UserNotFoundException(id);
        return subscribers;
    }

    @Override
    public void subscribe(long whoId, long onwhoId) throws UserNotFoundException, AlreadySubscribedException {
        User onwho = userDAO.find(onwhoId);

        if (userDAO.find(whoId) == null) throw new UserNotFoundException(whoId);
        if (onwho == null) throw new UserNotFoundException(onwhoId);
        if (userDAO.isSubscribed(whoId, onwhoId)) throw new AlreadySubscribedException(whoId, onwhoId);

        userDAO.subscribe(whoId, onwhoId);
    }

    @Override
    public List<User> getByName(String name) {
        return userDAO.searchByName(name);
    }

    @Override
    public User getByUsername(String name) {
        return userDAO.getByUsername(name);
    }

    @Override
    public void unsubscribe(long whoId, long onwhoId) throws UserNotFoundException, NotSubscribedException {
        User onwho = userDAO.find(onwhoId);

        if (userDAO.find(whoId) == null) throw new UserNotFoundException(whoId);
        if (onwho == null) throw new UserNotFoundException(onwhoId);
        if (!userDAO.isSubscribed(whoId, onwhoId)) throw new NotSubscribedException(whoId, onwhoId);

        userDAO.unsubscribe(whoId, onwhoId);
        
    }

    @Override
    public User save(User myUser) {
        return repository.save(myUser);
    }

    @Override
    @Transactional
    public void refreshToken(String username, String token) {
        User user = getByUsername(username);
        user.setFcmToken(token);
        repository.save(user);
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
