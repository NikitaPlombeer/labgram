package com.sp.server.service.impl;

import com.sp.server.dao.UserDAO;
import com.sp.server.entity.User;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.service.FeedService;
import com.sp.server.utils.FeedItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sharadbrat on 08.03.17.
 */
@Service
public class FeedServiceImpl implements FeedService {

    private static final Logger logger = LoggerFactory.getLogger(FeedServiceImpl.class);

    private UserDAO userDAO;

    @Override
    public List<FeedItem> getFeed(String username) throws UserNotFoundException {
        logger.debug("{} get feed {}", username, System.currentTimeMillis());
        User user = userDAO.getByUsername(username);
        if (user == null) {
            throw new UserNotFoundException(username);
        }

        return userDAO.getFeed(user.getId());
    }

    @Override
    public List<FeedItem> getFeedAfter(String username, Long imageId) throws UserNotFoundException {
        logger.debug("{} get feed {}", username, System.currentTimeMillis());
        User user = userDAO.getByUsername(username);
        if (user == null) {
            throw new UserNotFoundException(username);
        }

        return userDAO.getFeedAfter(user.getId(), imageId);
    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
