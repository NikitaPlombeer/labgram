package com.sp.server.service.impl;

import com.sp.server.config.HttpConfig;
import com.sp.server.entity.Image;
import com.sp.server.entity.User;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.service.NotificationService;
import com.sp.server.service.UserService;
import org.apache.http.entity.ContentType;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import org.apache.http.client.fluent.Request;

@Service
@PropertySource(value = {"classpath:app.properties"})
public class NotificationServiceImpl implements NotificationService {

    private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);
    private UserService userService;
    private HttpConfig httpConfig;
    private String serverFCMKey;
    private String senderId;

    @Override
    public void newImagePublished(Image image, User user) {
        try {
            List<User> subscribers = userService.getSubscribers(user.getId());
            for (User subscriber : subscribers) {
                if(subscriber.getFcmToken() != null) {
                    postMessageAboutNewFeed(image, user);
                }
            }
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void postMessageAboutNewFeed(Image image, User user) {
        JSONObject object = new JSONObject();
        object.put("from", senderId);
        object.put("to", user.getFcmToken());
        JSONObject notification = new JSONObject();
        notification.put("title", "New Image");
        notification.put("body", user.getUsername() + " post new image");
        object.put("notification", notification);

        JSONObject feedItem = new JSONObject();
        feedItem.put("userId", user.getId());
        feedItem.put("username", user.getUsername());
        feedItem.put("imageId", image.getId());
        feedItem.put("description", image.getDescription());
        feedItem.put("createdOn", image.getCreatedOn().getTime());

        JSONObject data = new JSONObject();
        data.put("type", "feed");
        data.put("item", feedItem.toString());
        object.put("data", data);


        String content = null;
        try {
            logger.info("POST {}", object.toString());
            content = Request.Post("https://fcm.googleapis.com/fcm/send")
                    .connectTimeout(httpConfig.getConnectTimeout())
                    .socketTimeout(httpConfig.getSocketTimeout())
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "key=" + serverFCMKey)
                    .bodyString(object.toString(), ContentType.APPLICATION_JSON)
                    .execute().returnContent().asString();
            JSONObject jsonObject = new JSONObject(content);
            logger.info(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Value("${server.fcm.key}")
    public void setServerFCMKey(String serverFCMKey) {
        this.serverFCMKey = serverFCMKey;
    }

    @Value("${server.fcm.sender.id}")
   public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    @Autowired
    public void setHttpConfig(HttpConfig httpConfig) {
        this.httpConfig = httpConfig;
    }
}
