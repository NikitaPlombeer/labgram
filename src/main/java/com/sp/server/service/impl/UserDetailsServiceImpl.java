package com.sp.server.service.impl;

import com.sp.server.entity.User;
import com.sp.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();
    private UserService userService;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userService.getByUsername(username);

        if (user == null)
            throw new UsernameNotFoundException("user not found");

        detailsChecker.check(user);
        return user;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}