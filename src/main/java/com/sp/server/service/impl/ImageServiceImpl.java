package com.sp.server.service.impl;

import com.sp.server.dao.ImageDAO;
import com.sp.server.dao.UserDAO;
import com.sp.server.entity.Image;
import com.sp.server.entity.User;
import com.sp.server.exception.ImageNotFoundException;
import com.sp.server.exception.ImageUploadException;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.exception.WrongFileFormatException;
import com.sp.server.service.ImageService;
import com.sp.server.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

/**
 * Created by sharadbrat on 27.02.17.
 */

@Service
public class ImageServiceImpl implements ImageService {

    private ImageDAO imageDAO;

    private UserDAO userDAO;

    private NotificationService notificationService;

    @Override
    public Image getById(long id) throws ImageNotFoundException {
        Image image = imageDAO.findOne2(id);
        if (image == null)
            throw new ImageNotFoundException(id);
        return image;
    }

    @Override
    public void save(MultipartFile file, String text, long userId) throws UserNotFoundException, ImageUploadException, WrongFileFormatException {

        try {

            User user = userDAO.find(userId);

            if (user == null)
                throw new UserNotFoundException(userId);

            if (!file.getContentType().startsWith("image/"))
                throw new WrongFileFormatException();

            Long imageId = imageDAO.saveImage(new Date(), text, file.getBytes());

            userDAO.addImage(userId, text, imageId);

            notificationService.newImagePublished(imageDAO.find(imageId), user);
        } catch (IOException e) {
            throw new ImageUploadException();
        }

    }

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }


    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Autowired
    public void setImageDAO(ImageDAO imageDAO) {
        this.imageDAO = imageDAO;
    }
}
