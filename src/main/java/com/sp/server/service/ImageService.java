package com.sp.server.service;

import com.sp.server.entity.Image;
import com.sp.server.exception.ImageNotFoundException;
import com.sp.server.exception.ImageUploadException;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.exception.WrongFileFormatException;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by sharadbrat on 27.02.17.
 */

public interface ImageService {

    Image getById(long id) throws ImageNotFoundException;
    void save(MultipartFile file, String text, long userId) throws UserNotFoundException, ImageUploadException, WrongFileFormatException;
}
