package com.sp.server.service;

import com.sp.server.entity.Image;
import com.sp.server.entity.User;

public interface NotificationService {

    void newImagePublished(Image image, User user);
}
