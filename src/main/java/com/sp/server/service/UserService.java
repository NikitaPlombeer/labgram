package com.sp.server.service;

import com.sp.server.entity.User;
import com.sp.server.exception.AlreadySubscribedException;
import com.sp.server.exception.NotSubscribedException;
import com.sp.server.exception.UserNotFoundException;

import java.util.List;

/**
 * Created by sharadbrat on 15.02.17.
 */
public interface UserService {

    User getById(long id) throws UserNotFoundException;

    List<User> getSubscriptions(long id) throws UserNotFoundException;

    List<User> getSubscribers(Long id) throws UserNotFoundException;

    List<User> getByName(String name);

    User getByUsername(String name);

    void subscribe(long whoId, long onwhoId) throws UserNotFoundException, AlreadySubscribedException;

    void unsubscribe(long whoId, long onwhoId) throws UserNotFoundException, NotSubscribedException;

    User save(User myUser);

    void refreshToken(String username, String token);
}
