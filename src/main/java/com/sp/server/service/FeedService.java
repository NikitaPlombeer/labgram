package com.sp.server.service;

import com.sp.server.exception.UserNotFoundException;
import com.sp.server.utils.FeedItem;

import java.util.List;

/**
 * Created by sharadbrat on 08.03.17.
 */
public interface FeedService {

    List<FeedItem> getFeed(String username) throws UserNotFoundException;

    List<FeedItem> getFeedAfter(String username, Long imageId) throws UserNotFoundException;
}
