package com.sp.server.controller;

import com.sp.server.exception.UserNotFoundException;
import com.sp.server.service.FeedService;
import com.sp.server.utils.FeedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by sharadbrat on 08.03.17.
 */
@RestController
@RequestMapping("/api")
public class FeedController {

    private FeedService feedService;

    @RequestMapping("/feed")
    public ResponseEntity<?> getFeed(
            @RequestParam(name = "imageId", required = false) Long imageId,
            Principal principal) throws UserNotFoundException {
        List<FeedItem> feed;
        if(imageId == null)
            feed = feedService.getFeed(principal.getName());
        else
            feed = feedService.getFeedAfter(principal.getName(), imageId);

        return new ResponseEntity<>(feed, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }


    @Autowired
    public void setFeedService(FeedService feedService) {
        this.feedService = feedService;
    }
}
