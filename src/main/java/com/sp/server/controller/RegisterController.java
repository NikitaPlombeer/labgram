package com.sp.server.controller;

import com.sp.server.entity.User;
import com.sp.server.entity.enums.UserRole;
import com.sp.server.exception.AlreadySubscribedException;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.service.UserService;
import com.sp.server.utils.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegisterController {

    private UserService service;

    private ShaPasswordEncoder shaPasswordEncoder;

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> register(@RequestBody User myUser) throws AlreadySubscribedException, UserNotFoundException {
        String username = myUser.getUsername();
        String password = myUser.getPassword();

        if (username == null || password == null)
            return new ResponseEntity<>(new ErrorResponse("Username and password are required"), HttpStatus.BAD_REQUEST);

        User user = service.getByUsername(username);
        if (user != null)
            return new ResponseEntity<>(new ErrorResponse("A user with this username already exists"), HttpStatus.FOUND);

        if (password.length() < 6)
            return new ResponseEntity<>(new ErrorResponse("Password can not be less than 6 characters"), HttpStatus.BAD_REQUEST);

        myUser.setPassword(shaPasswordEncoder.encodePassword(password, null));
        myUser.grantRole(UserRole.USER);

        User savedUser = service.save(myUser);
        service.subscribe(savedUser.getId(), savedUser.getId());
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @Autowired
    public void setService(UserService service) {
        this.service = service;
    }

    @Autowired
    public void setShaPasswordEncoder(ShaPasswordEncoder shaPasswordEncoder) {
        this.shaPasswordEncoder = shaPasswordEncoder;
    }
}