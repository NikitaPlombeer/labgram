package com.sp.server.controller;

import com.sp.server.entity.Image;
import com.sp.server.entity.User;
import com.sp.server.exception.ImageNotFoundException;
import com.sp.server.exception.ImageUploadException;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.exception.WrongFileFormatException;
import com.sp.server.service.ImageService;
import com.sp.server.service.UserService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.sql.SQLException;

/**
 * Created by sharadbrat on 27.02.17.
 */
@RestController
@RequestMapping(value = "/image")
public class ImageController {

    private final String type = "image/jpeg";
    private ImageService imageService;
    private UserService userService;

    @RequestMapping(value = "/{id}")
    public void getImage(@PathVariable("id") long id, HttpServletResponse response) throws ImageNotFoundException {
        Image image = imageService.getById(id);
        createAvatarDownloadingResponse(response, image);
    }

    @RequestMapping(value = "/user/{username}")
    public void getAvatar(@PathVariable("username") String username, HttpServletResponse response) throws ImageNotFoundException {
        User user = userService.getByUsername(username);
        Image image = user.getAvatar();
        createAvatarDownloadingResponse(response, image);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "multipart/form-data; charset=UTF-8")
    public ResponseEntity<?> uploadImage(@RequestParam("text") String text, @RequestParam("id") long userId, @RequestParam("file") MultipartFile file) throws UserNotFoundException, ImageUploadException, WrongFileFormatException {
        imageService.save(file, text, userId);
        return new ResponseEntity<>("Successfully uploaded an image!", HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ImageNotFoundException.class)
    public String imageNotFoundError(ImageNotFoundException e) {
        return "Image with id = " + e.getId() + " not found!";
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(WrongFileFormatException.class)
    public String wrongFileFormatError(WrongFileFormatException e) {
        return "Can upload only images!";
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(UserNotFoundException.class)
    public String userNotFoundError(UserNotFoundException e) {
        return "User with id = " + e.getId() + " not found!";
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ImageUploadException.class)
    public String ImageUploadError(ImageUploadException e) {
        return "Cannot upload image!";
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    private void createAvatarDownloadingResponse(HttpServletResponse response, Image image) {
        try {
            OutputStream out = response.getOutputStream();
            response.setContentType(type);
            IOUtils.copy(image.getValue().getBinaryStream(), out);
            out.flush();
            out.close();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }
}