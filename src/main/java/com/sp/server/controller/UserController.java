package com.sp.server.controller;

import com.sp.server.entity.User;
import com.sp.server.exception.AlreadySubscribedException;
import com.sp.server.exception.NotSubscribedException;
import com.sp.server.exception.UserNotFoundException;
import com.sp.server.service.UserService;
import com.sp.server.utils.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

/**
 * Created by sharadbrat on 15.02.17.
 */
@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    private UserService service;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) throws UserNotFoundException {
        User user = service.getById(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/subscriptions/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> getSubscriptions(@PathVariable("id") Long id) throws UserNotFoundException {
        List<User> subscriptions = service.getSubscriptions(id);
        return new ResponseEntity<>(subscriptions, HttpStatus.OK);
    }

    @RequestMapping(value = "/subscribers/{id}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> getSubscribers(@PathVariable("id") Long id) throws UserNotFoundException {
        List<User> subscribers = service.getSubscribers(id);
        return new ResponseEntity<>(subscribers, HttpStatus.OK);
    }

    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> getUsersByName(@PathVariable("name") String name) {
        List<User> searchRes = service.getByName(name);
        System.out.println(name);
        return new ResponseEntity<>(searchRes, HttpStatus.OK);
    }

    @RequestMapping(value = "/fcm", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> refreshToken(@RequestParam("token") String token, Principal principal) throws UserNotFoundException, AlreadySubscribedException {
        service.refreshToken(principal.getName(), token);
        return new ResponseEntity<>(Collections.singletonMap("result", "OK"), HttpStatus.OK);
    }

    @RequestMapping(value = "/subscribe", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> subscribe(@RequestParam("who") long whoId, @RequestParam("onwho") long onwhoId) throws UserNotFoundException, AlreadySubscribedException {
        service.subscribe(whoId, onwhoId);
        return new ResponseEntity<>("Successfully subscribed " + whoId + " on " + onwhoId + "!", HttpStatus.OK);
    }

    @RequestMapping(value = "/unsubscribe", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> unsubscribe(@RequestParam("who") long whoId, @RequestParam("onwho") long onwhoId) throws UserNotFoundException, NotSubscribedException {
        service.unsubscribe(whoId, onwhoId);
        return new ResponseEntity<>("Successfully unsubscribed " + whoId + " from " + onwhoId + "!", HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<?> userNotFoundError(UserNotFoundException e) {
        return new ResponseEntity<>(new ErrorResponse("User with id = " + e.getId() + " not found!"), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(AlreadySubscribedException.class)
    public ResponseEntity<?> alreadySubscribedError(AlreadySubscribedException e) {
        return new ResponseEntity<>(new ErrorResponse("User " + e.getWhoId() + " is already subscribed on " + e.getOnwhoId() + "!"), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(NotSubscribedException.class)
    public ResponseEntity<?> notSubscribedError(NotSubscribedException e) {
        return new ResponseEntity<>(new ErrorResponse("User " + e.getWhoId() + " is not subscribed on " + e.getOnwhoId() + "!"), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }


    @Autowired
    public void setService(UserService service) {
        this.service = service;
    }
}
