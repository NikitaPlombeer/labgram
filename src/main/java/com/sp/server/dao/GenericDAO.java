package com.sp.server.dao;

/**
 * Created by sharadbrat on 27.02.17.
 */
import java.util.List;

public interface GenericDAO<E, K> {

    void add(E entity);

    void update(E entity);

    void remove(E entity);

    E find(K key);

    List<E> list();

}