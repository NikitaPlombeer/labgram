package com.sp.server.dao;

import com.sp.server.entity.Image;

import java.util.Date;

/**
 * Created by sharadbrat on 27.02.17.
 */
public interface ImageDAO extends GenericDAO<Image, Long> {

    Long saveImage(Date date, String description, byte[] bytes);

    Image findOne2(Long id);
}
