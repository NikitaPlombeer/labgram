package com.sp.server.dao;

import com.sp.server.utils.FeedItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeedMapper implements RowMapper<FeedItem> {

    @Override
    public FeedItem mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        long userId = resultSet.getLong("user_id");
        String username = resultSet.getString("username");
        long imageId = resultSet.getLong("image_id");
        long date = resultSet.getDate("createdOn").getTime();
        String description = resultSet.getString("description");
        return new FeedItem(userId, username, imageId, date, description);
    }
}
