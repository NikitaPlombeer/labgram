package com.sp.server.dao;

import com.google.common.io.CharStreams;
import com.google.common.io.Closeables;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SqlCalls {

    public static final String RETRIEVE_USER_FEED_AFTER_ID = "db/sql/user_feed_after_id.sql";
    public static final String RETRIEVE_USER_FEED = "db/sql/user_feed.sql";

    private SqlCalls() {
    }

    /**
     * Method takes a file path String and returns the String representation of
     * that sql file
     *
     * @param filePath of location of the sql file
     * @return a String of the contents of the SQL file
     */
    public static String getSql(String filePath) {
        String sql = "";

        try {
            //get the file stream object
            InputStream is = SqlCalls.class.getClassLoader().getResourceAsStream(filePath);

            //convert to String and close stream
            sql = CharStreams.toString(new InputStreamReader(is));
            if (sql.isEmpty()) {
                Closeables.closeQuietly(is);
                throw new IOException("File path to SQL file could not be read!");
            }

            Closeables.closeQuietly(is);

            return sql;
        } catch (IOException ex) {
            //log your error
            return sql;
        }
    }
}
