package com.sp.server.dao;

import com.sp.server.entity.User;
import com.sp.server.utils.FeedItem;

import java.util.List;

/**
 * Created by sharadbrat on 27.02.17.
 */
public interface UserDAO extends GenericDAO<User, Long> {

    List<User> searchByName(String name);

    List<User> getSubscriptions(long id);

    List<User> getSubscribers(long id);

    void addImage(long userId, String text, long imageId);

    void subscribe(long whoId, long onwhoId);

    boolean isSubscribed(long whoId, long onwhoId);

    void unsubscribe(long whoId, long onwhoId);

    User getByUsername(String name);

    List<FeedItem> getFeed(Long id);
    List<FeedItem> getFeedAfter(Long userId, Long afterImageId);

}
