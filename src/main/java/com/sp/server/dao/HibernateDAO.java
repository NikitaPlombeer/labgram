package com.sp.server.dao;

/**
 * Created by sharadbrat on 27.02.17.
 */

import com.sp.server.utils.FeedItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Basic DAO operations dependent with Hibernate's specific classes
 *
 * @see SessionFactory
 */
@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public abstract class HibernateDAO<E, K extends Serializable> implements GenericDAO<E, K> {

    protected Class<? extends E> daoType;

    private SessionFactory sessionFactory;

    public HibernateDAO() {
        daoType = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session currentSession() {
        return sessionFactory.openSession();
    }

    @Override
    public void add(E entity) {
        Session session = currentSession();
        session.save(entity);
        session.close();
    }

    @Override
    public void update(E entity) {
        Session session = currentSession();
        session.saveOrUpdate(entity);
        session.close();
    }

    @Override
    public void remove(E entity) {
        Session session = currentSession();
        session.delete(entity);
        session.close();
    }

    @Override
    public E find(K key) {
        try (Session session = currentSession()) {
            return session.get(daoType, key);
        }
    }

    @Override
    public List<E> list() {
        try (Session session = currentSession()) {
            return session.createCriteria(daoType).list();
        }
    }
}