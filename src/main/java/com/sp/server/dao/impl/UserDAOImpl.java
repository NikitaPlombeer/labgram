package com.sp.server.dao.impl;

import com.sp.server.dao.HibernateDAO;
import com.sp.server.dao.FeedMapper;
import com.sp.server.dao.SqlCalls;
import com.sp.server.dao.UserDAO;
import com.sp.server.entity.User;
import com.sp.server.service.impl.FeedServiceImpl;
import com.sp.server.utils.FeedItem;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by sharadbrat on 27.02.17.
 */
@Repository
public class UserDAOImpl extends HibernateDAO<User, Long> implements UserDAO {

    private static final Logger logger = LoggerFactory.getLogger(FeedServiceImpl.class);
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public UserDAOImpl(DataSource dataSource) {
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<User> searchByName(String name) {
        Session session = currentSession();
        List<User> list = session.getNamedQuery("searchByName")
                .setParameter("username", name).list();
        session.close();
        return list;
    }

    @Override
    public List<User> getSubscriptions(long id) {
        Session session = currentSession();
        List<User> list = session.getNamedQuery("getSubscriptionsById")
                .setParameter("id", id).list();
        session.close();
        return list;
    }

    @Override
    public List<User> getSubscribers(long id) {
        Session session = currentSession();
        List<User> list = session.getNamedQuery("getSubscribersById")
                .setParameter("id", id).list();
        session.close();
        return list;
    }

    @Override
    public void addImage(long userId, String text, long imageId) {
        Session session = currentSession();
        session.beginTransaction();

        session.getNamedQuery("insertUserImage")
                .setParameter(0, userId)
                .setParameter(1, imageId)
                .executeUpdate();

        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void subscribe(long whoId, long onwhoId) {
        Session session = currentSession();
        session.beginTransaction();

        session.getNamedQuery("subscribe")
                .setParameter(0, whoId)
                .setParameter(1, onwhoId)
                .executeUpdate();

        session.getTransaction().commit();
        session.close();
    }

    @Override
    public boolean isSubscribed(long whoId, long onwhoId) {
        Session session = currentSession();

        Query namedQuery = session.getNamedQuery("isSubscribed")
                .setParameter(0, whoId)
                .setParameter(1, onwhoId);

        BigInteger id = (BigInteger) namedQuery.uniqueResult();

        session.close();
        return id != null;
    }

    @Override
    public void unsubscribe(long whoId, long onwhoId) {
        Session session = currentSession();
        session.beginTransaction();

        session.getNamedQuery("unsubscribe")
                .setParameter(0, whoId)
                .setParameter(1, onwhoId)
                .executeUpdate();

        session.getTransaction().commit();
        session.close();
    }

    @Override
    public User getByUsername(String name) {
        Session session = currentSession();
        User user = (User) session.getNamedQuery("getByUsername")
                .setParameter("username", name).uniqueResult();
        session.close();
        return user;
    }

    private FeedItem parse(Object obj[]) {
        for (int i = 0; i < obj.length; i++) {
            logger.trace("parse " + i + ") " + obj[i]);
        }

        long userId = ((BigInteger) obj[0]).longValue();
        String username = (String) obj[1];
        long imageId = ((BigInteger) obj[2]).longValue();
        long createdOn = ((Timestamp) obj[3]).getTime();//new Date((T) obj[3]).getTime();
        String text = (String) obj[4];//new Date((T) obj[3]).getTime();
        return new FeedItem(userId, username, imageId, createdOn, text);
    }

    @Override
    public List<FeedItem> getFeed(Long userId) {

        String sql = SqlCalls.getSql(SqlCalls.RETRIEVE_USER_FEED);
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("who", userId);
        return jdbcTemplate.query(sql, parameters, new FeedMapper());
    }

    @Override
    public List<FeedItem> getFeedAfter(Long userId, Long afterImageId) {
        String sql = SqlCalls.getSql(SqlCalls.RETRIEVE_USER_FEED_AFTER_ID);
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("who", userId);
        parameters.addValue("imageId", afterImageId);
        return jdbcTemplate.query(sql, parameters, new FeedMapper());
    }
}
