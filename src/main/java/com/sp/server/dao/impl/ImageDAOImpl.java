package com.sp.server.dao.impl;

import com.sp.server.dao.HibernateDAO;
import com.sp.server.dao.ImageDAO;
import com.sp.server.entity.Image;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

/**
 * Created by sharadbrat on 27.02.17.
 */
@Repository
public class ImageDAOImpl extends HibernateDAO<Image, Long> implements ImageDAO {

    @Override
    public Long saveImage(Date date, String description, byte[] bytes) {
        Session session = currentSession();
        session.beginTransaction();

        Blob blob = Hibernate.getLobCreator(session).createBlob(bytes);

        Image image = new Image();
        image.setValue(blob);
        image.setCreatedOn(date);
        image.setDescription(description);
        Serializable save = session.save(image);

        session.getTransaction().commit();
        session.close();
        return (Long) save;
    }

    @Override
    public Image findOne2(Long id) {
        Session session = currentSession();
        session.beginTransaction();
        Image image = session.get(daoType, id);
        session.refresh(image);
        Blob value = image.getValue();
        session.getTransaction().commit();
        session.close();
        return image;
    }
}