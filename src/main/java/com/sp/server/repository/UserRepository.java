package com.sp.server.repository;

import com.sp.server.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sharadbrat on 08.03.17.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
