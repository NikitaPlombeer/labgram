package com.sp.server.utils;

/**
 * Created by sharadbrat on 08.03.17.
 */
public class FeedItem {

    private long userId;
    private String username;
    private long imageId;
    private String description;
    private long createdOn;

    public FeedItem(long userId, String username, long imageId, long createdOn, String description) {
        this.userId = userId;
        this.username = username;
        this.imageId = imageId;
        this.createdOn = createdOn;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }
}

