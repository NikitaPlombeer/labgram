package com.sp.server.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sp.server.entity.enums.UserRole;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
@NamedQueries({
        @NamedQuery(name = "getSubscribersById", query = "SELECT u.subscribers FROM User u WHERE u.id = :id"),
        @NamedQuery(name = "searchByName", query = "SELECT u FROM User u WHERE u.username LIKE CONCAT('%', :username,'%')"),
        @NamedQuery(name = "getSubscriptionsById", query = "SELECT u.subscriptions FROM User u WHERE u.id = :id"),
        @NamedQuery(name = "getByUsername", query = "SELECT u FROM User u WHERE u.username = :username")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "insertUserImage", query = "INSERT INTO user_images (user_id, image_id) VALUES (?, ?)"),
        @NamedNativeQuery(name = "subscribe", query = "INSERT INTO subs (who, onwho) VALUES (?, ?)"),
        @NamedNativeQuery(name = "isSubscribed", query = "SELECT s.who FROM subs s WHERE s.who = ? AND s.onwho = ?"),
        @NamedNativeQuery(name = "unsubscribe", query = "DELETE FROM subs WHERE who = ? AND onwho = ?"),
        @NamedNativeQuery(name = "getFeed", query = "SELECT users.id as user_id, users.username as username, " +
                "image.id as image_id, image.createdOn as createdOn, image.description as description " +
                "FROM user_images, users, image " +
                "WHERE user_id in (SELECT onwho FROM subs where (who = ?)) AND " +
                "user_id = users.id AND  image.id = image_id ORDER BY createdOn desc")
})
public class User implements UserDetails {

    @JsonIgnore
    @ManyToMany(cascade = javax.persistence.CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinTable(name = "subs",
            joinColumns = @JoinColumn(name = "who", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "onwho", referencedColumnName = "id"))
    private List<User> subscribers;
    @JsonIgnore
    @ManyToMany(cascade = javax.persistence.CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinTable(name = "subs",
            joinColumns = @JoinColumn(name = "onwho", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "who", referencedColumnName = "id"))
    private List<User> subscriptions;
    @JsonIgnore
    @ManyToMany(cascade = javax.persistence.CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinTable(name = "user_images",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "image_id", referencedColumnName = "id"))
    private List<Image> images;
    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "avatar_id", unique = true)
    private Image avatar;
    @Id
    @GenericGenerator(name = "kaugen", strategy = "increment")
    @GeneratedValue(generator = "kaugen")
    private Long id;
    @NotNull
    @Size(min = 4, max = 30)
    private String username;
    @NotNull
    @Size(min = 4, max = 100)
    private String password;
    @Transient
    private long expires;
    @NotNull
    private boolean accountExpired;
    @NotNull
    private boolean accountLocked;
    @NotNull
    private boolean credentialsExpired;
    @NotNull
    private boolean accountEnabled;
    @Transient
    private String newPassword;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<UserAuthority> authorities;

    private String fcmToken;

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    public User(String username, Date expires) {
        this.username = username;
        this.expires = expires.getTime();
    }

    public List<User> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<User> subscribers) {
        this.subscribers = subscribers;
    }

    public List<User> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<User> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Image getAvatar() {
        return avatar;
    }

    public void setAvatar(Image avatar) {
        this.avatar = avatar;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public String getNewPassword() {
        return newPassword;
    }

    @JsonProperty
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    @JsonIgnore
    public Set<UserAuthority> getAuthorities() {
        return authorities;
    }

    // Use Roles as external API
    public Set<UserRole> getRoles() {
        Set<UserRole> roles = EnumSet.noneOf(UserRole.class);
        if (authorities != null) {
            for (UserAuthority authority : authorities) {
                roles.add(UserRole.valueOf(authority));
            }
        }
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        for (UserRole role : roles) {
            grantRole(role);
        }
    }

    public void grantRole(UserRole role) {
        if (authorities == null) {
            authorities = new HashSet<>();
        }
        authorities.add(role.asAuthorityFor(this));
    }

    public void revokeRole(UserRole role) {
        if (authorities != null) {
            authorities.remove(role.asAuthorityFor(this));
        }
    }

    public boolean hasRole(UserRole role) {
        return authorities.contains(role.asAuthorityFor(this));
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return !accountExpired;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return !accountEnabled;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + getUsername();
    }
}