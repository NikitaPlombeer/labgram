package com.sp.server.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Date;

/**
 * Created by sharadbrat on 15.02.17.
 */
@Entity
@Table(name = "image")
public class Image {

    @Id
    @GenericGenerator(name = "kaugen", strategy = "increment")
    @GeneratedValue(generator = "kaugen")
    @Column(name = "id")
    private long id;
    private Blob value;
    private String description;
    private Date createdOn;

    public Image() {

    }

    public Image(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", createdOn=" + createdOn +
                '}';
    }

    @JsonValue
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "value")
    public Blob getValue() {
        return value;
    }

    public void setValue(Blob value) {
        this.value = value;
    }

    @Column(name = "createdOn")
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}