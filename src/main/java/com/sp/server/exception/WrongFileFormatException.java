package com.sp.server.exception;

/**
 * Created by sharadbrat on 02.03.17.
 */
public class WrongFileFormatException extends Exception {
    public WrongFileFormatException() {
        super();
    }

    public WrongFileFormatException(String message) {
        super(message);
    }

    public WrongFileFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongFileFormatException(Throwable cause) {
        super(cause);
    }

    protected WrongFileFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
