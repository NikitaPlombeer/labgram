package com.sp.server.exception;

/**
 * Created by sharadbrat on 07.03.17.
 */
public class NotSubscribedException extends Exception {
    private long whoId;

    private long onwhoId;

    public NotSubscribedException(long whoId, long onwhoId) {
        super();
        this.onwhoId = onwhoId;
        this.whoId = whoId;
    }

    public NotSubscribedException(String message, long whoId, long onwhoId) {
        super(message);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public NotSubscribedException(String message, Throwable cause, long whoId, long onwhoId) {
        super(message, cause);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public NotSubscribedException(Throwable cause, long whoId, long onwhoId) {
        super(cause);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    protected NotSubscribedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, long whoId, long onwhoId) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public long getWhoId() {
        return whoId;
    }

    public long getOnwhoId() {
        return onwhoId;
    }
}
