package com.sp.server.exception;

/**
 * Created by sharadbrat on 06.03.17.
 */
public class AlreadySubscribedException extends Exception {
    private long whoId;

    private long onwhoId;

    public AlreadySubscribedException(long whoId, long onwhoId) {
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public AlreadySubscribedException(String message, long whoId, long onwhoId) {
        super(message);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public AlreadySubscribedException(String message, Throwable cause, long whoId, long onwhoId) {
        super(message, cause);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public AlreadySubscribedException(Throwable cause, long whoId, long onwhoId) {
        super(cause);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public AlreadySubscribedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, long whoId, long onwhoId) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.whoId = whoId;
        this.onwhoId = onwhoId;
    }

    public long getWhoId() {
        return whoId;
    }

    public long getOnwhoId() {
        return onwhoId;
    }
}
