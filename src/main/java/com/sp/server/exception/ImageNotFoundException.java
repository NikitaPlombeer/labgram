package com.sp.server.exception;

/**
 * Created by sharadbrat on 27.02.17.
 */
public class ImageNotFoundException extends Exception {
    private long id;

    public long getId() {
        return id;
    }

    public ImageNotFoundException(long id) {
        this.id = id;
    }

    public ImageNotFoundException(String message, long id) {
        super(message);
        this.id = id;
    }

    public ImageNotFoundException(String message, Throwable cause, long id) {
        super(message, cause);
        this.id = id;
    }

    public ImageNotFoundException(Throwable cause, long id) {
        super(cause);
        this.id = id;
    }

    public ImageNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, long id) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.id = id;
    }
}
