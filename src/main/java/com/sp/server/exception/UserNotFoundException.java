package com.sp.server.exception;

/**
 * Created by sharadbrat on 27.02.17.
 */
public class UserNotFoundException extends Exception {

    private Object id;

    public UserNotFoundException(Object id) {
        this.id = id;
    }

    public UserNotFoundException(String message, Object id) {
        super(message);
        this.id = id;
    }

    public UserNotFoundException(String message, Throwable cause, Object id) {
        super(message, cause);
        this.id = id;
    }

    public UserNotFoundException(Throwable cause, Object id) {
        super(cause);
        this.id = id;
    }

    public UserNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, long id) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.id = id;
    }

    public Object getId() {
        return id;
    }
}
