SELECT users.id as user_id,
  users.username as username,
  image.id as image_id,
  image.createdOn as createdOn,
  image.description as description
FROM user_images, users, image
WHERE user_id in
      (SELECT onwho FROM subs where (who = :who))
      AND user_id = users.id AND  image.id = image_id ORDER BY createdOn desc
LIMIT 10