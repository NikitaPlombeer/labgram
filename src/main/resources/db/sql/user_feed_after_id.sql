SELECT * FROM (SELECT
                 users.id          AS user_id,
                 users.username    AS username,
                 image.id          AS image_id,
                 image.createdOn   AS createdOn,
                 image.description AS description
               FROM user_images, users, image
               WHERE user_id IN
                     (SELECT onwho
                      FROM subs
                      WHERE (who = :who))
                     AND user_id = users.id AND image.id = image_id
) feed
  WHERE (feed.createdOn < (SELECT img.createdOn FROM image img WHERE img.id = :imageId LIMIT 1))
ORDER BY createdOn DESC
LIMIT 10;